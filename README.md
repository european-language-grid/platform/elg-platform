# ELG Platform

Welcome to the ELG (European Language Grid) GitLab repository.

This is the dedicated repository for discussions about the ELG  platform: https://gitlab.com/european-language-grid/platform/elg-platform.

The conversation takes is carried out in the form of issues; you can check previous discussions at: https://gitlab.com/european-language-grid/platform/elg-platform/-/issues, or create a new issue at: https://gitlab.com/european-language-grid/platform/elg-platform/-/issues/new.

We recommend the use of labels for easier tracking:
 - clarification: for asking questions on the use of the platform or metadata descriptions
 - bug: for bugs or technical issues you've run into while using the platform
 - feature: suggestions for improvements of existing features or adding new features.

You can also find more information on the use of the platform in the ELG User Manual at: https://european-language-grid.readthedocs.io.

The ELG team
